function showMB() {
    var matchContent = document.querySelector('.match-content');
    var tournamentContent = document.querySelector('.tournament-content');

    matchContent.style.display = 'block';
    tournamentContent.style.display = 'none';
}

function showTB() {
    var matchContent = document.querySelector('.match-content');
    var tournamentContent = document.querySelector('.tournament-content');

    matchContent.style.display = 'none';
    tournamentContent.style.display = 'block';
}

const image = document.getElementById("myImage");
const fileInput = document.getElementById("fileInput");

fileInput.addEventListener("change", function (event) {
    const selectedFile = event.target.files[0];
    if (selectedFile) {
        const objectURL = URL.createObjectURL(selectedFile);
        image.src = objectURL;
    }
});

function validateForm(event) {
    event.preventDefault();
    const name = document.getElementById("name").value;
    const phone = document.getElementById("phone").value;
    const calendar = document.getElementById("calendar").value;
    const scalendar = document.getElementById("scalendar").value;
    const ecalendar = document.getElementById("ecalendar").value;

    const modal = document.getElementById("myModal");
    const modalMessage = document.getElementById("modal-message");

    // Check if any of the required fields are empty
    if (name === "" && phone === "" && calendar === "" && scalendar === "" && ecalendar === "") {
        modalMessage.innerText = "Please fill in all required fields.";
        modal.style.display = "block";
    } else {
        const modal = document.getElementById("myModal");
        modal.style.display = "none";
        confirmationModal.style.display = "block";
        modal.style.display = "block";
        // }
    }
}

function closeModal() {
    const modal = document.getElementById("myModal");
    modal.style.display = "none";
}

function submitForm() {
        const successModal = document.getElementById("successModal");
        const successMessage = document.getElementById("successMessage");
        
        successMessage.innerText = "Form submitted successfully!";
        successModal.style.display = "block";
        
        closeConfirmationModal();
    }

    function closeSuccessModal() {
        const successModal = document.getElementById("successModal");
        successModal.style.display = "none";
        resetForm()
    }
    function closeConfirmationModal() {
        const confirmationModal = document.getElementById("confirmationModal");
        confirmationModal.style.display = "none";
        closeModal()
}


function resetForm() {
    const nameInput = document.getElementById("name");
    const phoneInput = document.getElementById("phone");
    const calendarInput = document.getElementById("calendar");
    const scalendarInput = document.getElementById("scalendar");
    const ecalendarInput = document.getElementById("ecalendar");

    // Set the value of each input element to an empty string
    nameInput.value = "";
    phoneInput.value = "";
    calendarInput.value = "";
    scalendarInput.value = "";
    ecalendarInput.value = "";
}