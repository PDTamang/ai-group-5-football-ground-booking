//////////////////////////////////////

// Function to show the confirmation dialog
function showConfirmationDialog() {
    document.getElementById("confirmationDialog").style.display = "block";
  }
  
  // Function to hide the confirmation dialog
  function hideConfirmationDialog() {
    document.getElementById("confirmationDialog").style.display = "none";
  }
  
  document.addEventListener("click", function (event) {
    if (event.target.classList.contains("cancel-button")) {
      showConfirmationDialog();
  
      // Store a reference to the specific "Cancel" button that triggered the dialog
      const cancelButton = event.target;
  
      document.getElementById("confirmCancel").addEventListener("click", function () {
        // Handle the cancellation logic here (e.g., removing the row)
        const row = cancelButton.closest("tr");
        row.remove(); // This line removes the entire row from the table.
  
        hideConfirmationDialog();
      });
  
      document.getElementById("cancelCancel").addEventListener("click", function () {
        hideConfirmationDialog();
      });
    }
  });
  