function updateDetails() {
    // Get the input field values
    var name = document.getElementById("name").value;
    var email = document.getElementById("email").value;
    var phone = document.getElementById("phone").value;
    var password = document.getElementById("password").value;

    // Perform some validation if needed
    if (name === "" || email === "" || phone === "" || password === "") {
        alert("Please fill in all the fields.");
        return;
    }

    // Perform the update (for demonstration, we'll just display the updated info)
    var updatedInfo = "Name: " + name + "<br>Email: " + email + "<br>Contact Number: " + phone + "<br>Password: (hidden for security)";
    document.getElementById("user-info").innerHTML = updatedInfo;

    // You can also send the updated data to a server here for actual data update.
    // For demonstration purposes, we are only updating the content on the page.
}
