
const email = localStorage.getItem('email');
window.onload = function(){
    fetch(`/api/v1/users/${email}`)
    .then((response) => response.text())
    .then((data) => getMe(data)) 
  }

function getMe(data){
    const mydata = JSON.parse(data);
    console.log(mydata)
    console.log(mydata.data)


    const name = document.getElementById('name');
    const email = document.getElementById('email');
    const contact = document.getElementById('phone');
    const proImg = document.getElementById("myImage")

    name.value = mydata.data.name
    email.value = mydata.data.email
    contact.value = mydata.data.phoneNo

    console.log(mydata.data.photo)
    proImg.src = mydata.data.photo
    

}


const image = document.getElementById("myImage");
const paymentPic = document.getElementById("fileInput");

paymentPic.addEventListener("change", function (event) {
    const selectedFile = event.target.files[0];
    if (selectedFile) {
        console.log("selectedFile: "+selectedFile)
        const objectURL = URL.createObjectURL(selectedFile);
        console.log(objectURL)
      
        image.src = objectURL;
    }
});
const form = document.getElementById('updating-f');
form.addEventListener("submit", function(event) {
    event.preventDefault(); // Prevents the form from submitting the traditional way
    const email = document.getElementById('email');
    
    const formData = new FormData(form);

    fetch(`/api/v1/users/${email}`, {
        method: "PATCH", 
        body: formData
    })
    .then((response) => {
        if (response.status === 200) {
            alert("Successfully updated");
            localStorage.setItem("email",email.value)
            alert(email.value)
            window.location.href = "/profile.html";
            


        } else if(response.status === 404) {
            alert("User not found");

        } else {
            alert(`Update failed with status: ${response.status}`);
        }
    })
    .catch((error) => {
        alert("error here")
        console.error("Error during update:", error);
    });


    
});









