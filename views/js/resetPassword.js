// const { reset } = require("nodemon");


function update() {
    // Call validateForm to get the email and password
    var formValues = validateForm();

    if (formValues) {
        var email = formValues.email;
        var password = formValues.password;

        // Here, you can access both email and password
        console.log("Updating email:", email);
        console.log("Updating password:", password);

        reset();
    }
}
function validateForm() {

    var password = document.getElementById("password").value;
    var email = document.getElementById("email").value;
    var confirmPassword = document.getElementById("confirmPassword").value;
    // Check if the email follows a valid format
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        if (!email.match(emailPattern)) {
            alert("Please enter a valid email address.");
            return;
        }

        else if (password === "" || email === "" || confirmPassword === "") {
            alert("Please fill in all fields.");
            return;
        }
        else if (password.trim().length < 6 || password.trim().length > 20) {
             alert("Password min 6 max 20 charecters.")
          }
        else if (password !== confirmPassword) {
            alert("Passwords do not match. Please try again.");
            return;
        }  
       else{
        return { email: email, password: password };S
       }
    }

    function reset() {
        // Clear the input fields
        document.getElementById("password").value = "";
        document.getElementById("email").value = "";
        document.getElementById("confirmPassword").value = "";
    
        // Clear any error messages (if you have error messages displayed)
        var errorMessages = document.getElementsByClassName("error-message");
        for (var i = 0; i < errorMessages.length; i++) {
            errorMessages[i].innerText = "";
        }
    }
    
