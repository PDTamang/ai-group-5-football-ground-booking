/////////////////////////////////////

// Function to show the confirmation dialog
function showConfirmationDialog() {
    document.getElementById("confirmationDialog").style.display = "block";
  }
  
  // Function to hide the confirmation dialog
  function hideConfirmationDialog() {
    document.getElementById("confirmationDialog").style.display = "none";
  }
  
  document.addEventListener("click", function (event) {
    if (event.target.classList.contains("cancel-button")) {
      showConfirmationDialog();
  
      // Store a reference to the specific "Cancel" button that triggered the dialog
      const cancelButton = event.target;
  
      document.getElementById("confirmCancel").addEventListener("click", function () {
        // Handle the cancellation logic here (e.g., removing the row)
        const row = cancelButton.closest("tr");
        row.remove(); // This line removes the entire row from the table.
  
        hideConfirmationDialog();
      });
  
      document.getElementById("cancelCancel").addEventListener("click", function () {
        hideConfirmationDialog();
      });
    }
  });

 // Function to open the reschedule modal and populate it with current values
function toggleButton(button) {
  const rescheduleModal = document.getElementById("rescheduleModal");
  rescheduleModal.style.display = "block";
  currentButton = button; // Store the current button for reference

  // Get the current date and time values from the row
  const row = button.closest("tr");
  const currentDate = row.querySelector("td[data-label='Event Date']").textContent;
  const currentTime = row.querySelector("td[data-label='Time']").textContent;

  // Populate the modal form with the current values
  document.getElementById("newDate").value = currentDate;
  document.getElementById("newTime").value = currentTime;
}

// Function to submit the reschedule action
function submitReschedule() {
  // Get the updated date and time values from the modal form
  const newDate = document.getElementById("newDate").value;
  const newTime = document.getElementById("newTime").value;

  // Update the reservation with the new date and time (You need to implement this logic)
  
  // Close the reschedule modal
  closeRescheduleModal();
}
