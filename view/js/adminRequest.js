  


function showConfirmationDialog() {
    document.getElementById("confirmationDialog").style.display = "block";
  }
  
  function hideConfirmationDialog() {
    // Reset the input value
    // document.getElementById("messageInput").value = "";
    // alert("oii")
  
    document.getElementById("confirmationDialog").style.display = "none";
  }
  
  document.addEventListener("click", function (event) {
    if (event.target.classList.contains("accept-button")) {
      // Handle the accept logic here (e.g., removing the row)
      // showConfirmationDialog();
      // alert("oii")
      
      const row = event.target.closest("tr");
      const idElement = row.querySelector(".eleid").textContent;
      // alert(idElement)


   

      fetch(`/api/v1/booking/${idElement}`, {
        method: "PATCH", 
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({
            bookingStatus: true
        })
    })
    .then((response) => {
        if (response.status === 200) {
            alert("Request accepted");
            row.remove();
            row = "";
        } else if(response.status === 500) {
            alert("Could not accept");
        } else {
            alert(`Update failed with status: ${response.status}`);
        }
    })
    .catch((error) => {
        // alert("Error here");
        console.error("Error during update:", error);
    });


    row = "";
    } else if (event.target.classList.contains("decline-button")) {
      showConfirmationDialog();
    }
  });
  
  // Set up the event listener for the "sendMessage" button outside of the click event
  document.getElementById("sendMessage").addEventListener("click", function () {
      // const row = document.querySelector(".accept-button").closest("tr");
      const row = event.target.closest("tr");

      const idElement = row.querySelector(".eleid").textContent;
      // alert(idElement)

      fetch(`/api/v1/booking/${idElement}`, {
        method: "DELETE",
    })
    .then((response) => {
        if (response.status === 204) {
            alert("Request deleted successfully");
            row.remove();
            row = "";

        } else if(response.status === 500) {
            alert("Could not delete the request");
        } else {
            alert(`Deletion failed with status: ${response.status}`);
        }
    })
    .catch((error) => {
        // alert("Error here");
        console.error("Error during deletion:", error);
    });
    
    row = "";
      hideConfirmationDialog();
  });
  
  document.getElementById("cancelSend").addEventListener("click", function () {
    hideConfirmationDialog();
  });
  

var viewImageLinks = document.querySelectorAll('.viewImageLink');
var centeredImages = document.querySelectorAll('.centered-image');

for (var i = 0; i < viewImageLinks.length; i++) {
  viewImageLinks[i].addEventListener('click', function(event) {
    event.preventDefault();

    // Hide all other images
    for (var j = 0; j < centeredImages.length; j++) {
      centeredImages[j].style.display = 'none';
    }

    // Show the corresponding image
    var index = Array.from(viewImageLinks).indexOf(this);
    centeredImages[index].style.display = 'block';
  });
}


window.onload = function(){
  fetch(`/api/v1/booking/nsbs`)
  .then((response) => response.text())
  .then((data) => getRequestsBookings(data)) 
}



async function getRequestsBookings(mybooks) {
  const bookdata = JSON.parse(mybooks);
  console.log(bookdata)
  count = 0;
  arr = []
  console.log(bookdata.data)

  for(i=bookdata.data.length-1; i>=0;i--){
    arr[count++] = bookdata.data[i];
  }
  console.log(arr)
  arr.forEach((bookings) => {
    V = bookings
    console.log(V)
    var megaContainer = document.querySelector("tbody");;
    if(bookings.bookingType  == "match"){
      // alert("m int")
      var tableRowsData = MtableRows(bookings)
      console.log(tableRowsData)
      megaContainer.appendChild(tableRowsData)
    }else{
      var tableRowsData = TtableRows(bookings)
      console.log(tableRowsData)
      megaContainer.appendChild(tableRowsData)
    }
  });

}

function MtableRows(bookings){
  // Create the table row
var tableRow = document.createElement("tr");

// Create and append the first cell with text content
var nameCell = document.createElement("td");
nameCell.textContent = bookings.name;
nameCell.setAttribute("text", "name");
tableRow.appendChild(nameCell);

// Create and append the second cell with text content
var idCell = document.createElement("td");
idCell.textContent = bookings.phoneNo;
tableRow.appendChild(idCell);

// Create and append the third cell with data-label attribute and text content
var dateCell = document.createElement("td");
dateCell.setAttribute("data-label", "Event Date");
dateCell.textContent = bookings.matchDate.slice(0, 10);;
tableRow.appendChild(dateCell);

// Create and append the fourth cell with data-label attribute and text content
var timeCell = document.createElement("td");
timeCell.setAttribute("data-label", "Time");
timeCell.textContent = timeIn12hr(bookings);
tableRow.appendChild(timeCell);

// Create and append the fifth cell with data-label attribute and a link
var paymentCell = document.createElement("td");
var paymentLink = document.createElement("a");
paymentLink.href = bookings.paymentPic;
paymentLink.id = "viewImageLink";
paymentLink.style.color = "rgb(19, 119, 173)";
paymentLink.textContent = "View Image";
paymentCell.appendChild(paymentLink);
tableRow.appendChild(paymentCell);

// Create and append the sixth cell with two buttons
var actionCell = document.createElement("td");
var acceptButton = document.createElement("button");
acceptButton.className = "accept-button";
acceptButton.textContent = "Accept";
var declineButton = document.createElement("button");
declineButton.className = "decline-button";
declineButton.textContent = "Decline";
actionCell.appendChild(acceptButton);
actionCell.appendChild(declineButton);
tableRow.appendChild(actionCell);


var idd = document.createElement("td");
idd.classList.add("eleid")

idd.textContent = bookings._id;
idd.style.display = "none";

tableRow.appendChild(idd);






  return tableRow
}

function TtableRows(bookings){

  // Create the table row
  var tableRow = document.createElement("tr");

  // Create and append the first cell with text content
  var nameCell = document.createElement("td");
  nameCell.textContent = bookings.name;
  nameCell.setAttribute("text", "name");
  tableRow.appendChild(nameCell);
  
  // Create and append the second cell with text content
  var idCell = document.createElement("td");
  idCell.textContent = bookings.phoneNo;
  tableRow.appendChild(idCell);
  
  // Create and append the third cell with data-label attribute and text content
  var dateCell = document.createElement("td");
  dateCell.setAttribute("data-label", "Event Date");
  dateCell.textContent = dateGet(bookings);
  tableRow.appendChild(dateCell);
  
  // Create and append the fourth cell with data-label attribute and text content
  var timeCell = document.createElement("td");
  timeCell.setAttribute("data-label", "Time");
  timeCell.textContent = TtimeIn12hr(bookings);
  tableRow.appendChild(timeCell);
  
  // Create and append the fifth cell with data-label attribute and a link
  var paymentCell = document.createElement("td");
  var paymentLink = document.createElement("a");
  paymentLink.href = bookings.paymentPic
;
  paymentLink.id = "viewImageLink";
  paymentLink.style.color = "rgb(19, 119, 173)";
  paymentLink.textContent = "View Image";
  paymentCell.appendChild(paymentLink);
  tableRow.appendChild(paymentCell);
  
  // Create and append the sixth cell with two buttons
  var actionCell = document.createElement("td");
  var acceptButton = document.createElement("button");
  acceptButton.className = "accept-button";
  acceptButton.textContent = "Accept";
  var declineButton = document.createElement("button");
  declineButton.className = "decline-button";
  declineButton.textContent = "Decline";
  actionCell.appendChild(acceptButton);
  actionCell.appendChild(declineButton);
  tableRow.appendChild(actionCell);


  var idd = document.createElement("td");
  idd.classList.add("eleid")


  idd.textContent = bookings._id;
  idd.style.display = "none";
tableRow.appendChild(idd);

  
    return tableRow
}

function dateGet(bookings){
  var sdate = bookings.startingTournamentDate.slice(0, 10)
  var edate = bookings.endingTournamentDate.slice(0, 10)

  return `${sdate}- ${edate}`
}

function checkRemark(bookings){
  const check = bookings.bookingStatus;
  let remark = "";
  if (check){
    remark = "SUCCESSFULLY RESERVED"
  }else{
    remark = "PENDING..."
  }
  return remark;
}

function timeIn12hr(bookings){
  // Get the selected date value from the input element
  var stime = bookings.SmatchTime;
  stime = convertime(stime)
  var etime = bookings.EmatchTime
  etime = convertime(etime)
  return `${stime} - ${etime}`
}
function TtimeIn12hr(bookings){
  // Get the selected date value from the input element
  var stime = bookings.startingTournamentTime;
  stime = convertime(stime)
  var etime = bookings.endingTournamentTime

  etime = convertime(etime)
  return `${stime} - ${etime}`
}

function convertime(time){
  const [hrs, mins] = time.split(':');
  let adjustedHours = Number(hrs);

  if (adjustedHours > 12) {
      adjustedHours = adjustedHours - 12;
      time = adjustedHours.toString().padStart(2, '0') + ":" + mins + " PM";
  } else {
      time = adjustedHours.toString().padStart(2, '0') + ":" + mins + " AM";
  }

  return time
}

