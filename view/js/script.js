const form = document.querySelector("#log");
const emailInput = document.getElementById("login-email");
const passwordInput = document.getElementById("login-password");
// const loginBtn = document.getElementById("loginBtn")

form.addEventListener("submit", function(event) {
    event.preventDefault(); // Prevent the form from submitting by default
    login(event); // Pass the event to the login function
});

function login(event) {
    // console.log("hi hi")
    validateForm();
    if(isFormValid()) {
        form.submit();
    } 
}

function isFormValid() {
    const inputContainers = form.querySelectorAll(".field");
    let result = true;
    inputContainers.forEach((container) => {
      if (container.classList.contains("error")) {
        result = false;
      }
      return result;
    });
    
  }

function validateForm() {

    //EMAIL
    if (emailInput.value.trim() == "") {
      setError(emailInput, "Provide email address");
    } else if (isEmailValid(emailInput.value)) {
      setSuccess(emailInput);
    } else {
      setError(emailInput, "Provide valid email address");
    }
  
    //PASSWORD
    if (passwordInput.value.trim() == "") {
      setError(passwordInput, "Password can not be empty");
    } else if (
      passwordInput.value.trim().length < 6 ||
      passwordInput.value.trim().length > 20
    ) {
      setError(passwordInput, "Password min 6 max 20 charecters");
    } else {
      setSuccess(passwordInput);
    }
  }

  function setError(element, errorMessage) {
    const parent = element.parentElement;
    if (parent.classList.contains("success")) {
      parent.classList.remove("success");
    }
    parent.classList.add("error");
    const paragraph = parent.querySelector("p");
    paragraph.textContent = errorMessage;
  }
  
  function setSuccess(element) {
    const parent = element.parentElement;
    if (parent.classList.contains("error")) {
      parent.classList.remove("error");
    }
    parent.classList.add("success");
  }

  function isEmailValid(email) {
    const reg =
      /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    return reg.test(email);
  }
  

  const signupForm = document.querySelector("#sign")
const emailInput2 = document.getElementById("signup-email");
const passwordInput2 = document.getElementById("signup-password");
const confirmPasswordInput = document.getElementById("signup-confrim-password");

signupForm.addEventListener("submit", function(event) {
    event.preventDefault(); // Prevent the form from submitting by default
    signup(event); // Pass the event to the login function
});

function signup(event) {
    console.log("hiiiii")
    loginValidateForm();
    if(isFormValid()) {
        form.submit();

    } 
}

function loginValidateForm() {

    //EMAIL
    if (emailInput2.value.trim() == "") {
      setError(emailInput2, "Provide email address");
    } else if (isEmailValid(emailInput2.value)) {
      setSuccess(emailInput2);
    } else {
      setError(emailInput2, "Provide valid email address");
    }
  
    //PASSWORD
    if (passwordInput2.value.trim() == "") {
      setError(passwordInput2, "Password can not be empty");
    } else if (
      passwordInput.value.trim().length < 6 ||
      passwordInput.value.trim().length > 20
    ) {
      setError(passwordInput2, "Password min 6 max 20 charecters");
    } else {
      setSuccess(passwordInput2);
    }

    // Confirm password
    if (confirmPasswordInput.value.trim() == "") {
        setError(confirmPasswordInput, "Confirm password can not be empty");
      } else if (confirmPasswordInput.value !== passwordInput.value){
        setError(confirmPasswordInput, "Passwords do not match");
      } else {
        setSuccess(confirmPasswordInput);
      }
  }