const email = localStorage.getItem('email');
// alert(email)

window.onload = function(){
    fetch(`/api/v1/users/${email}`)
    .then((response) => response.text())
    .then((data) => getMe(data)) 
  }

  function getMe(data){
    const mydata = JSON.parse(data);
    console.log(mydata)
    console.log(mydata.data)


    const name = document.getElementById('name');
    const phone = document.getElementById('phoneNo');

    name.value = mydata.data.name
    phone.value = mydata.data.phoneNo
   
    

}

function showMB() {
    console.log("in showmb")

    var matchContent = document.querySelector('.match-content');
    var tournamentContent = document.querySelector('.tournament-content');

    matchContent.style.display = 'block';
    tournamentContent.style.display = 'none';
}

function showTB() {
    console.log("in showtb")
    var matchContent = document.querySelector('.match-content');
    var tournamentContent = document.querySelector('.tournament-content');

    matchContent.style.display = 'none';
    tournamentContent.style.display = 'block';
}

// const image = document.getElementById("myImages");
// const paymentPic = document.getElementById("fileInput");

// paymentPic.addEventListener("change", function (event) {
//     const selectedFile = event.target.files[0];
//     if (selectedFile) {
//         console.log("selectedFile: "+selectedFile)
//         const objectURL = URL.createObjectURL(selectedFile);
//         console.log(objectURL)
      
//         image.src = objectURL;
//     }
// });



function validateForm(event) {
    event.preventDefault();
    // alert("validation")
    const name = document.getElementById("name").value;
    const phone = document.getElementById("phoneNo").value;
    const matchDate = document.getElementById("matchDate").value;
    const mstime = document.getElementById("SmatchTime").value;
    const metime = document.getElementById("EmatchTime").value;
    const tstartcal = document.getElementById("startingTournamentDate");
    const tendcal = document.getElementById("endingTournamentDate");
    const tstartt = document.getElementById("startingTournamentTime");
    const tEndt = document.getElementById("endingTournamentTime");
    const pic = localStorage.getItem("bookingImg")
   
   
    // Check if any of the required fields are empty
    if (name === "" || phone === ""  && matchDate =="" && mstime == "" && metime == "" ){
        alert("Please fill in all required fields.")
        modalMessage.innerText = "Please fill in all required fields.";
        modal.style.display = "block";
    }else {
        const modal = document.getElementById("myModal");
        modal.style.display = "none";
        confirmationModal.style.display = "block";
        modal.style.display = "block";
        // }
    }
}

function closeModal() {
    const modal = document.getElementById("myModal");
    modal.style.display = "none";
}

function submitForm() {
    
        const successModal = document.getElementById("successModal");
        const successMessage = document.getElementById("successMessage");
        
        successMessage.innerText = "Form submitted successfully!";
        successModal.style.display = "block";


        const form = document.getElementById('booking-f');

        const matchDate = document.getElementById("matchDate").value;
        const inputElement = document.createElement("input");
        if (matchDate ===""){
            inputElement.setAttribute("type", "text");
        
            inputElement.setAttribute("name", "bookingType");
    
            inputElement.setAttribute("value", "tournament");
        
            inputElement.style.display = "none";
        
            form.appendChild(inputElement);
        }else{
            inputElement.setAttribute("type", "text");
        
            inputElement.setAttribute("name", "bookingType");
    
            inputElement.setAttribute("value", "match");
        
            inputElement.style.display = "none";
        
            form.appendChild(inputElement);
        }

        // const email = "pdt@gmail.com"

        const inputemail = document.createElement("input");
        inputemail.setAttribute("type", "text");
        inputemail.setAttribute("name", "email");
        inputemail.setAttribute("value", email);
        inputemail.style.display = "none";
        form.appendChild(inputemail);


        const formData = new FormData(form);

        const name = document.getElementById("name").value;
        const phone = document.getElementById("phoneNo").value;
        const mstime = document.getElementById("SmatchTime").value;
        const metime = document.getElementById("EmatchTime").value;
        const tstartcal = document.getElementById("startingTournamentDate").value;
        const tendcal = document.getElementById("endingTournamentDate").value;
        const tstartt = document.getElementById("startingTournamentTime").value;
        const tEndt = document.getElementById("endingTournamentTime").value;
        const pic = localStorage.getItem("bookingImg")



        console.log('Name: ', name);
        console.log('Phone: ', phone);
        console.log('Match Date: ', matchDate);
        console.log('Match Start Time: ', mstime);
        console.log('Match End Time: ', metime);
        console.log('Tournament Start Date: ', tstartcal);
        console.log('Tournament End Date: ', tendcal);
        console.log('Tournament Start Time: ', tstartt);
        console.log('Tournament End Time: ', tEndt);
        console.log('Booking Image: ', pic);
        console.log("bookingtype: ", inputElement)
        console.log("inputemail: ", inputemail)


        inputElement.remove();
        inputemail.remove();

        console.log(formData)
        console.log(inputElement.textContent)
    
        fetch('/api/v1/booking', {
          method: 'POST',
          body: formData
        })
        .then((response) => {
            if (response.status == 201){
                // alert("successfully booked") 
                
            }else if(response.status == 400){
                alert("Booking already exists for this date and time")
            }else if(response.status == 404){
                alert("Match date must be in today's date or future when provided")
            }else{
                if(response.status == 500){
                    alert("Server error!!!")
                }else{
                    alert("Something went wrong!!!")
                }
            };
        })

        closeConfirmationModal(); 
         

             
    }
    async function GetAllHouse(data) {
        const props = JSON.parse(data);
        console.log(data)
        alert(data)
      }

    function closeSuccessModal() {
        const successModal = document.getElementById("successModal");
        successModal.style.display = "none";
        resetForm()
    }
    function closeConfirmationModal() {
        const confirmationModal = document.getElementById("confirmationModal");
        confirmationModal.style.display = "none";
        closeModal()
}


function resetForm() {
    // const name = document.getElementById("name");
    // const phone = document.getElementById("phoneNo");
    const mstime = document.getElementById("SmatchTime");
    const metime = document.getElementById("EmatchTime");
    const tstartcal = document.getElementById("startingTournamentDate");
    const tendcal = document.getElementById("endingTournamentDate");
    const tstartt = document.getElementById("startingTournamentTime");
    const tEndt = document.getElementById("endingTournamentTime");
    // const image = document.getElementById("myImages");




    // Set the value of each input element to an empty string
    // name.value = "";
    // phone.value = "";
    mstime.value = "";
    metime.value = "";
    
    // image.src = "";

    tstartcal.value = "";
    tendcal.value = "";
    tstartt.value = "";
    tEndt.value = "";

}

function uploadImage() {
    const form = document.getElementById('imageForm');
    const formData = new FormData(form);

    fetch('/api/v1/booking', {
      method: 'POST',
      body: formData
    })
    .then(response => response.json())
    .then(data => {
      console.log('Image uploaded successfully:', data);
    })
    .catch(error => {
      console.error('Error uploading image:', error);
    });
  }


// Add event listeners after the DOM is fully loaded
document.getElementById('tournamentBookingButton').addEventListener('click', showTB);
document.getElementById('matchBookingButton').addEventListener('click', showMB);

document.getElementById('sendButton').addEventListener('click', validateForm);

document.getElementById('submitForm').addEventListener('click', submitForm);
document.getElementById('closeConfirmationModal').addEventListener('click', closeConfirmationModal);
document.getElementById('closeConfirmation').addEventListener('click', closeConfirmationModal);
document.getElementById('closeSuccessModal').addEventListener('click', closeSuccessModal);


