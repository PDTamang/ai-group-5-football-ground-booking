window.onload = function(){
    fetch('/api/v1/booking/sbs')
    .then((response) => response.text())
    .then((data) => getMyBookings(data))
  }
  
  async function getMyBookings(mybooks) {
    const bookdata = JSON.parse(mybooks);
    console.log(bookdata)
    count = 0;
    arr = []
    console.log(bookdata.data)
  
    for(i=bookdata.data.length-1; i>=0;i--){
      arr[count++] = bookdata.data[i];
    }
    console.log(arr)
    arr.forEach((bookings) => {
      console.log(bookings)
      var megaContainer = document.querySelector("tbody");
      if(bookings.bookingType  == "match"){
        // alert("m int")
        var tableRowsData = MtableRows(bookings)
        console.log(tableRowsData)
        megaContainer.appendChild(tableRowsData)
      }else{
        var tableRowsData = TtableRows(bookings)
        console.log(tableRowsData)
        megaContainer.appendChild(tableRowsData)
      }
    });
  }
  
  function MtableRows(bookings){
    var tRow = document.createElement("tr")
    var td1 = document.createElement("td");
    td1.setAttribute("data-label", "Event Date");
    td1.textContent = bookings.matchDate.slice(0, 10);
    tRow.appendChild(td1);
  
    var td2 = document.createElement("td");
    td2.setAttribute("data-label", "Time");
    td2.textContent = timeIn12hr(bookings);
    tRow.appendChild(td2);
  
    var td3 = document.createElement("td");
    td3.textContent = bookings.name;
    tRow.appendChild(td3);

    var td5 = document.createElement("td");
    td5.textContent = bookings.bookingType;
    tRow.appendChild(td5);
  
    var td4 = document.createElement("td");
    td4.textContent = bookings.phoneNo;
    tRow.appendChild(td4);
    
    return tRow
  }
  function TtableRows(bookings){
    var tRow = document.createElement("tr")
    var td1 = document.createElement("td");
    td1.setAttribute("data-label", "Event Date");
    td1.textContent = dateGet(bookings);
    tRow.appendChild(td1);
  
    var td2 = document.createElement("td");
    td2.setAttribute("data-label", "Time");
    td2.textContent = TtimeIn12hr(bookings);
    tRow.appendChild(td2);
  
    var td3 = document.createElement("td");
    td3.textContent = bookings.name;
    tRow.appendChild(td3);

    var td5 = document.createElement("td");
    td5.textContent = bookings.bookingType;
    tRow.appendChild(td5);
  
    var td4 = document.createElement("td");
    td4.textContent = bookings.phoneNo;
    tRow.appendChild(td4);
    
    return tRow
  }
  function dateGet(bookings){
    var sdate = bookings.startingTournamentDate.slice(0, 10)
  var edate = bookings.endingTournamentDate.slice(0, 10)
    return `${sdate}- ${edate}`
  }

  

  function checkRemark(bookings){
    const check = bookings.bookingStatus;
    let remark = "";
    if (check){
      remark = "SUCCESSFULLY RESERVED"
    }else{
      remark = "PENDING..."
    }
    return remark;
  }
  
  function timeIn12hr(bookings){
    // Get the selected date value from the input element
    var stime = bookings.SmatchTime;
    stime = convertime(stime)
    var etime = bookings.EmatchTime
    etime = convertime(etime)
    return `${stime} - ${etime}`
  }
  function TtimeIn12hr(bookings){
    // Get the selected date value from the input element
    var stime = bookings.startingTournamentTime
    stime = convertime(stime)
    var etime = bookings.endingTournamentTime
    etime = convertime(etime)
    return `${stime} to ${etime}`
  }


  function convertime(time){
    const [hrs, mins] = time.split(':');
    let adjustedHours = Number(hrs);
  
    if (adjustedHours > 12) {
        adjustedHours = adjustedHours - 12;
        time = adjustedHours.toString().padStart(2, '0') + ":" + mins + " PM";
    } else {
        time = adjustedHours.toString().padStart(2, '0') + ":" + mins + " AM";
    }
  
    return time
  }
  
  
      // Add a click event listener to the search icon
      document.getElementById('searchIcon').addEventListener('click', function() {
        // Get the selected date from the date input
        var selectedDate = document.getElementById('matchDate').value;
  


    fetch(`/api/v1/booking/getByDate/${selectedDate}`)
      .then((response) => response.text())
      .then((data) => getMyBookings2(data))
      });

      
async function getMyBookings2(mybooks) {
  const bookdata = JSON.parse(mybooks);
  console.log(bookdata)
  
  // Clear the existing table content
  var megaContainer = document.querySelector("tbody");
  megaContainer.innerHTML = '';



  count = 0;
  arr = []
  console.log(bookdata.data)
  console.log(bookdata)

  for (i = bookdata.data.length - 1; i >= 0; i--) {
      arr[count++] = bookdata.data[i];
  }
  console.log(arr.length)
  if (arr.length === 0){
    var tRow = document.createElement("tr")
    var td1 = document.createElement("td");
    td1.setAttribute("data-label", "Event Date");
    td1.textContent = dateGet(bookdata.data);
    tRow.appendChild(td1);
  
    var td2 = document.createElement("td");
    td2.setAttribute("data-label", "Time");
    td2.textContent = TtimeIn12hr(bookdata.data);
    tRow.appendChild(td2);
  
    var td3 = document.createElement("td");
    td3.textContent = bookdata.data.name;
    tRow.appendChild(td3);

    var td5 = document.createElement("td");
    td5.textContent = bookdata.data.bookingType;
    tRow.appendChild(td5);
  
    var td4 = document.createElement("td");
    td4.textContent = bookdata.data.phoneNo;
    tRow.appendChild(td4);

    megaContainer.appendChild(tRow)
    
  }else{
    arr.forEach((bookings) => {
      console.log(bookings)
      if (bookings.bookingType == "match") {
          var tableRowsData = MtableRows(bookings);
          console.log(tableRowsData);
          megaContainer.appendChild(tableRowsData);
      } 
  });
  }

}


document.getElementById('refresh').addEventListener('click', reloadPage);
function reloadPage() {
  // Reload the page
  location.reload();
}