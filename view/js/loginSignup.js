localStorage.setItem('email',"");

document.getElementById("signup-form").addEventListener("submit", function(event) {
    event.preventDefault(); // Prevents the form from submitting the traditional way

    var Uname = document.getElementById("uname").value;
    var Uemail = document.getElementById("uemail").value;
    var UphoneNo = document.getElementById("phoneNo").value;
    var Upassword = document.getElementById("upassword").value;
    var UconfirmPassword = document.getElementById("uconfirm-password").value;

    // Validate name
    if (!Uname) {
        document.querySelector(".errorUn").textContent = "Please enter your name.";
        return;
    } else {
        document.querySelector(".errorUn").textContent = "";
    }

    // Validate email
    if (!validateEmail(Uemail)) {
        document.querySelector(".errorUe").textContent = "Wrong email";
        return;
    } else {
        document.querySelector(".errorUe").textContent = "";
    }

    // Validate phone number
    if (!validatePhoneNo(UphoneNo)) {
        return;
    } else {
        document.querySelector(".errorUpn").textContent = "";
    }

    // Validate password
    if (!validatePassword(Upassword)) {
        return;
    } else {
        document.querySelector(".errorUpss").textContent = "";
    }

    // Validate confirm password
    if (Upassword !== UconfirmPassword) {
        document.querySelector(".errorCpss").textContent = "Password does not match";
        return;
    } else {
        document.querySelector(".errorCpss").textContent = "";
    }


    //=== server side =====
    const signupData = {
        name : Uname,
        email : Uemail,
        phoneNo :UphoneNo,
        password : Upassword,
        passwordConfirm : UconfirmPassword
    };
    console.log(signupData)


    fetch("/api/v1/users/signup", {
        method: "POST",
        headers: { "Content-Type": "application/json"},
        body: JSON.stringify(signupData)
    })
    .then((response) => {
        if (response.status == 201){
            alert("successfully signuped")
                       // Get the label element
                       const label = document.querySelector('label[for="flip"]');
                       // Toggle visibility by checking its current state and applying the opposite
                       label.style.display = label.style.display === "none" ? "block" : "none";
        }
        // alert(response.status)
        // if (response.status ==500) {
        //     alert ("server error")
        // }

    })

    .catch(error => {
        alert("could not signup here")
        console.error(error);
    });


    
});


function validateEmail(email) {
    // Regular expression for validating an Email
    var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return re.test(email);
}

function validatePhoneNo(phoneNo) {
    // Validates if phone number is 8 digits long
    const isValidPhone = /^\d{8}$/.test(phoneNo);
    // Displays an error message if the phone number is invalid
    if (!isValidPhone) {
        document.querySelector(".errorUpn").textContent = "Invalid phone number. Please enter a valid 8-digit phone number.";
    } else {
        document.querySelector(".errorUpn").textContent = "";
    }
    return isValidPhone;
}

function validatePassword(password) {
    // Validates password with specific criteria
    const isValidPassword = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$/.test(password);
    // Displays an error message if the password is invalid
    if (!isValidPassword) {
        document.querySelector(".errorUpss").textContent = "Minimum 8 characters, 1 uppercase, 1 lowercase, 1 number, and 1 special character (!@#$%^&*).";
    } else {
        document.querySelector(".errorUpss").textContent = "";
    }
    return isValidPassword;
}
function validateEmail(email) {
    // Regular expression for validating an Email
    var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return re.test(email);
}

function validatePhoneNo(phoneNo) {
    const isValidPhone = /^(17|77)\d{6}$/.test(phoneNo);
    if (!isValidPhone) {
        document.querySelector(".errorUpn").textContent = "Should start with 17 or 77 and have 8-digit in total"
    } else {
        document.querySelector(".errorUpn").textContent = "";
    }
    return isValidPhone;
}


function validatePassword(password) {
    const isValidPassword = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$/.test(password);
    if (!isValidPassword) {
        document.querySelector(".errorUpss").textContent = "Minimum 8 characters, 1 uppercase, 1 lowercase, 1 number, and 1 special character (!@#$%^&*).";
    }else {
        document.querySelector(".errorUpss").textContent ="";

    }
    return isValidPassword;
}


//================ LOGIN ============================

document.getElementById("login-form").addEventListener("submit", function(event) {
    event.preventDefault(); // Prevents the form from submitting the traditional way

    var Uemail = document.getElementById("login-email").value;
    var Upassword = document.getElementById("login-password").value;

    // Validate email
    if (!validateLoginEmail(Uemail)) {
        document.querySelector(".errorLe").textContent = "Wrong email";
        return;
    } else {
        document.querySelector(".errorLe").textContent = "";
    }


    // Validate password
    if (!validateLoginPassword(Upassword)) {
        return;
    } else {
        document.querySelector(".errorLpss").textContent = "";
    }

    //=== server side =====

    var loginData = {
        email: Uemail,
        password: Upassword,
    };
    console.log(loginData)
    fetch("/api/v1/users/login", {
        method: "POST",
        headers: { "Content-Type": "application/json"},
        body: JSON.stringify(loginData)
    })
    .then((response) => {
        if (response.status == 200){
            window.location.href = "/index2.html"; 
            // Set a key-value pair in localStorage
            localStorage.setItem('email', loginData.email);

        }else if(response.status == 202){
            window.location.href = "/adminIndex2.html"; 
            localStorage.setItem('email', loginData.email);
        }else{
            alert(response.status, "incorrect password or email")
        }
        // alert("your status was: "+response.status)

    })

    .catch(error => {
        alert("could not login")
        console.error(error);
    });

    
});

function validateLoginPassword(password) {
    // Validates password with specific criteria
    const isValidPassword = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$/.test(password);
    // Displays an error message if the password is invalid
    if (!isValidPassword) {
        document.querySelector(".errorLpss").textContent = "Minimum 8 characters, 1 uppercase, 1 lowercase, 1 number, and 1 special character (!@#$%^&*).";
    } else {
        document.querySelector(".errorLpss").textContent = "";
    }
    return isValidPassword;
}
function validateLoginEmail(email) {
    // Regular expression for validating an Email
    var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return re.test(email);
}
