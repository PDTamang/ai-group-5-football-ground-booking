const email = localStorage.getItem('email');
window.onload = function(){
    fetch(`/api/v1/users/${email}`)
    .then((response) => response.text())
    .then((data) => getMe(data)) 
  }

function getMe(data){
    const mydata = JSON.parse(data);
    console.log(mydata)
    console.log(mydata.data)


    const name = document.querySelector('.name span');
    const email = document.querySelector('.email span');
    const contact = document.querySelector('.contact span');
    const proImg = document.getElementById("myImage")

    name.textContent = mydata.data.name
    email.textContent = mydata.data.email
    contact.textContent = mydata.data.phoneNo
    console.log(mydata.data.photo)
    proImg.src = mydata.data.photo
}