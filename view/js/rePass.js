
document.getElementById("resPass").addEventListener("submit", function(event) {
    
    event.preventDefault(); 
    var email = document.getElementById("remail").value;

    var data = {
        email: email,
    };

    fetch("/api/v1/users/fp", {
        method: "POST",
        headers: { "Content-Type": "application/json"},
        body: JSON.stringify(data)
    })
 
    .then((response) => {
        if (response.status == 200){
         alert("successfully sent the request to your email")
        }else if (response.status == 404) {
            alert("User not found")
        }else {
            alert("some thing went went worng.")
        }

    })

    .catch(error => {
        alert("error sending email")
        console.error(error);
    });
});