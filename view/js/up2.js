
const email = localStorage.getItem('email');
// alert(email)
window.onload = function(){
    fetch(`/api/v1/users/${email}`)
    .then((response) => response.text())
    .then((data) => getMe(data)) 
  }

function getMe(data){
    const mydata = JSON.parse(data);
    console.log(mydata)
    console.log(mydata.data)


    const name = document.getElementById('name');
    const email = document.getElementById('email');
    const contact = document.getElementById('phoneNo');
    const proImg = document.getElementById("myImage")

    name.value = mydata.data.name
    email.value = mydata.data.email
    contact.value = mydata.data.phoneNo

    console.log(mydata.data.photo)
    proImg.src = mydata.data.photo
    

}


// const image = document.getElementById("myImage");
// const paymentPic = document.getElementById("photo");

// paymentPic.addEventListener("change", function (event) {
//     const selectedFile = event.target.files[0];
//     if (selectedFile) {
//         console.log("selectedFile: "+selectedFile)
//         const objectURL = URL.createObjectURL(selectedFile);
//         console.log(objectURL)
      
//         image.src = objectURL;
//     }
// });




function submitForm() {
  const form = document.getElementById('updating-f');
  const formData = new FormData(form);

  fetch(`/api/v1/users/${email}`, {
    method: "PATCH", 
    body: formData
})
.then((response) => {
    if (response.status === 200) {
        alert("Successfully updated");
        // localStorage.setItem("email",email)
        // alert(email.value)
        window.location.href = "/adminPprofile.html";
        


    } else if(response.status === 404) {
        alert("User not found");

    } else {
        alert(`Update failed with status: ${response.status}`);
    }
})
.catch((error) => {
    alert("error here")
    console.error("Error during update:", error);
});
}

document.getElementById('submitForm').addEventListener('click', submitForm);

