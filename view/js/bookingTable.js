/////////////////////////////////////
//============== get form the data ======================
const email = localStorage.getItem('email');
window.onload = function(){
  fetch(`/api/v1/booking/${email}`)
  .then((response) => response.text())
  .then((data) => getMyBookings(data)) 
}

async function getMyBookings(mybooks) {
  const bookdata = JSON.parse(mybooks);
  console.log(bookdata)
  count = 0;
  arr = []
  console.log(bookdata.data)

  for(i=bookdata.data.length-1; i>=0;i--){
    arr[count++] = bookdata.data[i];
  }
  console.log(arr)
  arr.forEach((bookings) => {
    V = bookings
    console.log(V)
    var megaContainer = document.querySelector("tbody");;
    if(bookings.bookingType  == "match"){
      // alert("m int")
      var tableRowsData = MtableRows(bookings)
      console.log(tableRowsData)
      megaContainer.appendChild(tableRowsData)
    }else{
      var tableRowsData = TtableRows(bookings)
      console.log(tableRowsData)
      megaContainer.appendChild(tableRowsData)
    }
  });
  // let div = document.getElementById("yes")
  // div.style.marginTop = "px"; 
  // div.textContent = V
}

function MtableRows(bookings){
  var tRow = document.createElement("tr")
  var td1 = document.createElement("td");
  td1.setAttribute("data-label", "Event Date");
  td1.textContent = bookings.matchDate.slice(0, 10);
  tRow.appendChild(td1);

  var td2 = document.createElement("td");
  td2.setAttribute("data-label", "Time");
  td2.textContent = timeIn12hr(bookings);
  tRow.appendChild(td2);

  var td6 = document.createElement("td");
  td6.textContent = bookings.name;
  tRow.appendChild(td6);

  var td7 = document.createElement("td");
  td7.textContent = bookings.phoneNo;
  tRow.appendChild(td7);

  var td8 = document.createElement("td");
  td8.textContent = bookings.bookingType;
  tRow.appendChild(td8);

  var td3 = document.createElement("td");
  td3.setAttribute("data-label", "Remarks");
  td3.textContent = checkRemark(bookings);
  tRow.appendChild(td3);

  var td4 = document.createElement("td");
   if(!bookings.bookingStatus){
    var cancelButton = document.createElement("button");
    cancelButton.className = "cancel-button";
    cancelButton.textContent = "Cancel";
    td4.appendChild(cancelButton);

  }
  tRow.appendChild(td4);

  var td5 = document.createElement("td")
  td5.classList.add("id")
  td5.textContent = bookings._id;
  tRow.appendChild(td5)
  td5.style.display = "none";

  return tRow
}

function TtableRows(bookings){
  var tRow = document.createElement("tr")
  var td1 = document.createElement("td");
  td1.setAttribute("data-label", "Event Date");
  td1.textContent = dateGet(bookings);
  tRow.appendChild(td1);

  var td2 = document.createElement("td");
  td2.setAttribute("data-label", "Time");
  td2.textContent = TtimeIn12hr(bookings);
  tRow.appendChild(td2);

  var td6 = document.createElement("td");
  td6.textContent = bookings.name;
  tRow.appendChild(td6);

  var td7 = document.createElement("td");
  td7.textContent = bookings.phoneNo;
  tRow.appendChild(td7);

  var td8 = document.createElement("td");
  td8.textContent = bookings.bookingType;
  tRow.appendChild(td8);

  var td3 = document.createElement("td");
  td3.setAttribute("data-label", "Remarks");
  td3.textContent = checkRemark(bookings);
  tRow.appendChild(td3);

  var td4 = document.createElement("td");
   if(!bookings.bookingStatus){
    var cancelButton = document.createElement("button");
    cancelButton.className = "cancel-button";
    cancelButton.textContent = "Cancel";
    td4.appendChild(cancelButton);

  }
  tRow.appendChild(td4);

  var td5 = document.createElement("td")
  td5.classList.add("id")
  td5.textContent = bookings._id;
  tRow.appendChild(td5)
  td5.style.display = "none";

  return tRow
}

function dateGet(bookings){
  var sdate = bookings.startingTournamentDate.slice(0, 10)
  var edate = bookings.endingTournamentDate.slice(0, 10)

  return `${sdate}- ${edate}`
}

function checkRemark(bookings){
  const check = bookings.bookingStatus;
  let remark = "";
  if (check){
    remark = "SUCCESSFULLY RESERVED"
  }else{
    remark = "PENDING..."
  }
  return remark;
}

function timeIn12hr(bookings){
  // Get the selected date value from the input element
  var stime = bookings.SmatchTime;
  stime = convertime(stime)
  var etime = bookings.EmatchTime
  etime = convertime(etime)
  return `${stime} - ${etime}`
}
function TtimeIn12hr(bookings){
  // Get the selected date value from the input element
  var stime = bookings.startingTournamentTime;
  stime = convertime(stime)
  var etime = bookings.endingTournamentTime

  etime = convertime(etime)
  return `${stime} - ${etime}`
}

function convertime(time){
  const [hrs, mins] = time.split(':');
  let adjustedHours = Number(hrs);

  if (adjustedHours > 12) {
      adjustedHours = adjustedHours - 12;
      time = adjustedHours.toString().padStart(2, '0') + ":" + mins + " PM";
  } else {
      time = adjustedHours.toString().padStart(2, '0') + ":" + mins + " AM";
  }

  return time
}





// Function to show the confirmation dialog
function showConfirmationDialog() {
    document.getElementById("confirmationDialog").style.display = "block";
  }
  
  // Function to hide the confirmation dialog
  function hideConfirmationDialog() {
    document.getElementById("confirmationDialog").style.display = "none";
  }
  
  document.addEventListener("click", function (event) {
    if (event.target.classList.contains("cancel-button")) {
      showConfirmationDialog();
  
      // Store a reference to the specific "Cancel" button that triggered the dialog
      const cancelButton = event.target;
  
      document.getElementById("confirmCancel").addEventListener("click", function () {
        // Handle the cancellation logic here (e.g., removing the row)
        const row = cancelButton.closest("tr");
        const idElement = row.querySelector(".id").textContent;

        // alert(idElement)
        fetch(`/api/v1/booking/${idElement}`, {
          method: "DELETE",
          headers: {
              "Content-Type": "application/json"
          }
      })
      .then((response) => {
          if (response.status == 204) {
              row.remove(); // This line removes the entire row from the table.
              alert("booking canceled successfully");
          } else if (response.status == 500) {
              alert("booking not found");
          } else {
              alert("Failed to delete user. Status: " + response.status);
          }
      })
      .catch((error) => {
          alert("An error occurred while deleting user");
          console.error(error);
      }); 
        hideConfirmationDialog();
      });
  
      document.getElementById("cancelCancel").addEventListener("click", function () {
        hideConfirmationDialog();
      });
    }
  });

 // Function to open the reschedule modal and populate it with current values
function toggleButton(button) {
  const rescheduleModal = document.getElementById("rescheduleModal");
  rescheduleModal.style.display = "block";
  currentButton = button; // Store the current button for reference

  // Get the current date and time values from the row
  const row = button.closest("tr");
  const currentDate = row.querySelector("td[data-label='Event Date']").textContent;
  const currentTime = row.querySelector("td[data-label='Time']").textContent;

  // Populate the modal form with the current values
  document.getElementById("newDate").value = currentDate;
  document.getElementById("newTime").value = currentTime;
}

// Function to submit the reschedule action
function submitReschedule() {
  // Get the updated date and time values from the modal form
  const newDate = document.getElementById("newDate").value;
  const newTime = document.getElementById("newTime").value;

  // Update the reservation with the new date and time (You need to implement this logic)
  
  // Close the reschedule modal
  closeRescheduleModal();
}
