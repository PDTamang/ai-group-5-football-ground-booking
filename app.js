const express = require("express");
const app = express();
const userRouter = require('./routes/userRoutes');
const viewRouter = require("./routes/viewRoutes");
const bookingRouter = require("./routes/bookingRoutes");
const groundRouter = require("./routes/groundRoute")
const cors = require('cors');
const helmet = require('helmet');
const path = require('path');

app.use(cors());
app.use(helmet());
app.use(express.json());


app.use(express.urlencoded({ extended: true }));
// app.use(express.static('profileImg'));
// app.use(express.static('payImgs'));
app.use(express.static(path.join(__dirname, 'Images'))); // Serve images from 'payImgs' in the 'view' folder
// app.use(express.static('view'));
app.use(express.static('view'));
app.use('/api/v1/users', userRouter);
app.use("/", viewRouter);
app.use('/api/v1/booking', bookingRouter);

app.use("/api/v1/ground", groundRouter)

// Error handling middleware
app.use((err, req, res, next) => {
  console.error(err.stack);
  res.status(500).send('Something went wrong!');
});

module.exports = app;
