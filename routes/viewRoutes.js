const express = require('express')
const router = express.Router()
const viewsController = require('./../controller/viewController')

router.get('/login', viewsController.getLoginForm)
router.get('/resetPassword', viewsController.getResetPasswordForm)

module.exports = router