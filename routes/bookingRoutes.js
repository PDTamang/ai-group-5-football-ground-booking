const express = require("express")
const bookingController = require("./../controller/bookingController")
const router = express.Router()
const multer = require('multer');
const path = require('path');

const Storage = multer.diskStorage({
    destination: "Images",
    filename: (req, file, cb) => {
      console.log(file)
      cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    },
  });

exports.upload = multer({ storage: Storage}).single("paymentPic"); 

const yourCSPMiddleware = (req, res, next) => {
  res.setHeader('Content-Security-Policy', "script-src 'self' 'unsafe-inline'; style-src 'self' 'unsafe-inline'; img-src 'self' data: blob:;");
  next();
};

router
    .route("/")
    .get(bookingController.getAllBookings)
    .post(yourCSPMiddleware, bookingController.createBooking)
    .delete(bookingController.deleteBooking)



router.route("/getByDate/:date").get(bookingController.getByDate)
router.route("/sbs").get(bookingController.selectedBookings);
router.route("/nsbs").get(bookingController.notSelectedBookings)
router.route("/:email").get(bookingController.getAllMyBookings)
router.route("/sbsDate/:matchDate").get(bookingController.selectedBookingsDates);


router
    .route("/:id")
    .get(bookingController.getBooking)
    .delete(bookingController.deleteBooking)
    .patch(bookingController.updateBooking)






module.exports = router

