const express = require("express")
const groundController = require("./../controller/groundController")
const router = express.Router()


router
    .route("/")
    .get(groundController.getGroundInfo)
    .post(groundController.createGoundInfo)
    .patch(groundController.updateGoundInfo)


module.exports = router

