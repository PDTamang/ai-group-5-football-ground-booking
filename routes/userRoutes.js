const express = require("express")
const userController = require("./../controller/userController")
const authController = require("./../controller/authController")
const router = express.Router()
const multer = require('multer');
const path = require('path');

const Storage = multer.diskStorage({
  destination: "Images",
  filename: (req, file, cb) => {
    console.log(file)
    cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
  },
});

exports.upload = multer({ storage: Storage}).single("photo"); 
// const yourCSPMiddleware = (req, res, next) => {
//   res.setHeader('Content-Security-Policy', "script-src 'self' 'unsafe-inline'; style-src 'self' 'unsafe-inline'; img-src* data: *;");
//   next();
// };



router.post('/signup', authController.signup)
router.post("/login", authController.login)

router
    .route("/")
    .get(userController.getAllUsers)
    .post(userController.createUser)

router
    .route("/:email")
    .get(userController.getUser)
    .delete(userController.deleteUser)
    .patch(userController.updateUser)

router.route("/fp").post(userController.fp)
router.route("/rp/:id/:tokened").post(userController.rpf)
router.route("/rp/:id/:tokened").get(userController.rp)
    



module.exports = router

