const mongoose = require("mongoose")
const validator = require("validator")
const bcrypt = require("bcryptjs")
const crypto = require("crypto")

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Please tell us your nam"]
    },
    email: {
        type: String,
        required: [true, "Please provide your email"],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, "Please provide a valid email"]
    },
    phoneNo: {
        type: Number,
        required: [true, "Please provide your phone number"],
        maxlength: 8,
        minlength: 8
    },
    bankAcc: Number, 
    photo: {
        type: String,
        default: "defaultUser.jpeg"
    },
    role: {
        type: String,
        enum: ["user", "admin"],
        default: "user"
    },
    password: {
        type: String,
        required: [true, "Please provide a password!"],
        minlength: 6,
        //password wont be included when we get the users
        select: false
    },
    passwordConfirm: {
        type: String,
        required: [true, "Please confirm your password"],
        validate: {
            //This only works on SAVE!!!
            validator: function (el) {
                return el === this.password
            },
            message: "Passwords are not the same"
        },
        select: false
    },
    // passwordChangedAt: Date,
    passwordResetToken: {
        type: String,
    },
    // passwordResetTokenExpire: Date,
    active: {
        type: Boolean,
        default: true,
        select: false
    }
})
// userSchema.pre("save", async function(next) {
//     //Only run this function if password was actually modified
//     if (!this.isModified("password")) return next()
    
//     //Hash the password with cost of 12
//     this.password = await bcrypt.hash(this.password, 12)
//     //Delete passwordConfirm field
//     this.passwordConfirm = undefined 
//     next()

//  })

//  userSchema.pre("findOneAndUpdate", async function(next) {
//     const update = this.getUpdate();

//     if (update.password !== "" &&
//         update.password !== undefined &&
//         update.password == update.passwordConfirm) {
        
//         // Hash the password with const of 12
//         this.getUpdate().password = await bcrypt.hash(update.password, 12)

//         // Delete passwordConfirm field
//         update.passwordConfirm = undefined
//         next()
//         } else {
//             next()
//         }
//  })

//  userSchema.methods.correctPassword = async function(candidatePassword, userPassword){
//     console.log("password compare")
//     return await bcrypt.compare(candidatePassword, userPassword)
//  }


const User = mongoose.model("User", userSchema)
module.exports = User