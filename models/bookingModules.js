const mongoose = require("mongoose");
const validator = require("validator")


const bookingSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: [true, "Please tell us your name"]
    },
    phoneNo: {
        type: Number,
        required: [true, "Please provide your phone number"],
        maxlength: 8,
        minlength: 8
    },
    bookingType: {
        type: String,
        enum: ["match", "tournament"],
        default: "match",
        required: [true, "Please fill the Booking-Type"]
    },
    bookingStatus: {
        type: Boolean,
        default: false,
    },
    time: Number,
    startingTournamentDate: {
        type: Date
    },
    endingTournamentDate: {
        type: Date
    },
    startingTournamentTime: {
        type: String
    },
    endingTournamentTime: {
        type: String
    },
    matchDate: {
        type: Date,    
    },
    
    SmatchTime: {
        type: String
    },
    EmatchTime: {
        type: String
    },
    paymentPic: {
        type: String,
        required: [true, "Please fill pay the bill"] 
    }
});

const Booking = mongoose.model("Booking", bookingSchema);
module.exports = Booking;
