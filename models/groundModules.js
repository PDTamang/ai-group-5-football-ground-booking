const mongoose = require("mongoose")


const GroundSchema = new mongoose.Schema({
    name:{
        type:String
    },
    dayPrice:{
        type: Number
    },
    nightPrice:{
        type: Number
    }
})


const Ground = mongoose.model("Ground", GroundSchema)
module.exports = Ground