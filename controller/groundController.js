const { application } = require("express")
const AppError = require("../utils/appError")
const Ground = require("./../models/groundModules")


exports.createGoundInfo = async (req,res) => {
    try {
        const ground = await Ground.create(req.body)
        console.log(req.body.name)
        res.status(201).json({data: ground, status: "success"})
    } catch (err) {
        res.status(500).json({error: err.message})
    }
}

exports.updateGoundInfo = async (req,res) => {
    try {
        const id = "6554e46e28d1acfc264b129f";
        const ground = await Ground.findByIdAndUpdate(id, req.body);
        
        console.log(req.body.name)
        res.status(200).json({data: ground, status: "success"})
    } catch (err) {
        res.status(500).json({error: err.message})
    }
}

exports.getGroundInfo = async (req, res) => {
    try {
        const id = "6554e46e28d1acfc264b129f";
        const getGround = await Ground.findOne({ _id: id });
 

        res.status(200).json({data: getGround, status: "success"})
    } catch (err) {
        res.status(500).json({error: err.message})
    }
}



