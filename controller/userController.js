const { application } = require("express")
const AppError = require("../utils/appError")
const User = require("./../models/userModules")
const createUpload = require("./../routes/userRoutes")

const jwt = require("jsonwebtoken")
const bcrypt = require("bcryptjs")
const crypto = require('crypto')
const nodemailer = require("nodemailer")
const path = require('path')




exports.getAllUsers = async (req,res,next) => {
    try {
        const users = await User.find()
        res.status(200).json({data: users, status: "success"})
    } catch (err) {
        res.status(500).json({error: err.message})
    }
}

exports.createUser = async (req,res) => {
    try {
        const user = await User.create(req.body)
        console.log(req.body.name)
        res.json({data: user, status: "success"})
    } catch (err) {
        res.status(500).json({error: err.message})
    }
}

exports.getUser = async (req, res) => {
    try {
        const getUser = await User.findOne({ email: req.params.email }); 
        console.log("id: "+getUser._id)

        if (!getUser) {
          return res.status(404).json({ error: "User not found" });
        }
     
        res.json({data: getUser, status: "success"})
    } catch (err) {
        res.status(500).json({error: err.message})
    }
}

exports.updateUser = async (req, res) => {
    try {
        const getUser = await User.findOne({ email: req.params.email }); 
        console.log("id: "+getUser._id)

        if (!getUser) {
          return res.status(404).json({ error: "User not found" });
        }

        console.log("entering to handelField")
        await handleFileUpload(req, res);

        console.log("creating user")
        const updatedUser = createUser(req);

        logBookingData(updatedUser);
        
        console.log("updating")
        await checkAndUpdateUser(getUser, updatedUser);

        const user = await User.findByIdAndUpdate(getUser._id, req.body)
        res.status(200).json({data: user, status: "success"})
    } catch (err) {
        res.status(500).json({error: err.message})
    }
}

async function checkAndUpdateUser(getUser, updatedUser) {
  getUser.email = updatedUser.email;
  getUser.name = updatedUser.name;
  getUser.phoneNo = updatedUser.phoneNo;
  getUser.photo = updatedUser.photo;
  
  await getUser.save();
}

function logBookingData(updatedUser) {
  console.log("email: " + updatedUser.email);
  console.log("name: " + updatedUser.name);
  console.log("phoneNo: " + updatedUser.phoneNo);
  // ... (log other properties)
}

function createUser(req) {
  return new User({
      email: req.body.email,
      name: req.body.name,
      phoneNo: req.body.phoneNo,
      photo: req.file.filename,
  });
}

async function handleFileUpload(req, res) {
  return new Promise((resolve, reject) => {
      createUpload.upload(req, res, (err) => {
          if (err) {
              console.error("File upload error: ", err);
              reject(err);
          } else {
              resolve();
          }
      });
  });
}


exports.deleteUser = async (req, res) => {
    try {
        const user = await User.findByIdAndDelete(req.params.id)
        res.json({data: user, status: "success"})
    } catch (err) {
        res.status(500).json({error: err.message})
    }
}
// =============  Forgot password =================

exports.fp = async(req, res) => {
  const {email} = req.body;
  console.log(email+ "    123")
  try {
      const user = await User.findOne({email})
      console.log(user+ "non")
      if(!user) {
          return res.status(404).json({message: "User Not esist"})
      }
  
  const secret = process.env.JWT_SECRET+user.password
  const tokened = jwt.sign({email:user, id:user._id}, secret,{expiresIn: "5m"})

  console.log(email+"\n")

  const resetUrl =`${req.protocol}://${req.get('host')}/api/v1/users/rp/${user._id}/${tokened}`;
  console.log(resetUrl)
  var transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: {
        user: 'phurbadt2002@gmail.com',
        pass: 'vumhdsuglveswkqk'
      },
      html:true,
    });
    const mailTemplate = `
                          <h1>Password Reset</h1>

                          <a href="${resetUrl}">Reset Password</a>
    `
    var mailOptions = {
      from: 'phurbadt2002@gmail.com',
      to: email,
      subject: 'Password reset request',
      html:mailTemplate,
    };
    
    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
      } else {
        console.log('Email sent: ' + info.response);
        res.status(200).json({message:"successfully "})
      }
    });
    

  }catch(err){
      res.status(400).json({err:Error})
  }
}

exports.rp = async(req, res) => {
 
  const {id, tokened} = req.params;
  console.log(req.params)
  const user = await User.findOne({_id:id})
  if (!user){
      return res.json({status: "User not exizts"})
  }
  // res.statussend()
  console.log('redircting to a html')
  res.sendFile(path.join(__dirname, '../', 'view', 'reSetPass.html'));
  
 
}

exports.rpf = async(req, res) => {

  try {
    const {id, tokened} = req.params;
    const {password} = req.body;
    
    const user = await User.findOne({_id:id})
    if (!user){
        return res.json({status: "User not exizts"})
    }
    console.log(user)
    console.log("PASSWORD: "+password)
    if(password == ""){
      return res.status(402).json({status: "Passoword is empty"})
    }
      // const encryptPass = await bcrypt.hash(password, 10)
     
      await User.updateOne({_id:id},{$set:{password:password}})
      res.status(200).json({message:"password updated"})
  }catch(err){
      console.error("Error sending email4444:", err);
  }


exports.userDetailsByEmail = async (req, res) => {
    try {
      const email = req.params.email;
      const userDetails = async function getUserDetailsByEmail(email) {
          try {
            const user = await User.findOne({ email: email }).select('id passwordResetToken');
            if (user) {
              return { id: user._id, passwordResetToken: user.passwordResetToken };
            } else {
              return null; // User not found with the given email
            }
          } catch (error) {
            console.error('Error retrieving user details: ', error);
            throw error;
          }
        }
      
      if (userDetails) {
        res.status(200).json(userDetails);
      } else {
        // User not found, send a 404 response
        res.status(404).json({ error: "User not found" });
      }
    } catch (error) {
      // Handle errors and send a 500 response
      console.error("Error:", error);
      res.status(500).json({ error: "Internal Server Error" });
    }
    };
  
}
