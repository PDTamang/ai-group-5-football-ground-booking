// const { application } = require("express")
const Bookings = require("./../models/bookingModules")
const createUpload = require("./../routes/bookingRoutes")

exports.getAllBookings = async (req,res,next) => {
    try {
        const booking = await Bookings.find()
        res.status(200).json({data: booking, status: "success"})
    } catch (err) {
        res.status(500).json({error: err.message})
    }
}


exports.createBooking = async (req, res) => {
    try {
        await handleFileUpload(req, res);

        const bookingData = createBookingData(req);

        logBookingData(bookingData);

        if (bookingData.matchDate !== null && bookingData.matchDate < new Date().setHours(0, 0, 0, 0)) {
            return res.status(404).json({ error: "Match date must be in today's date or future when provided" });
        }

        await checkAndCreateBookings(bookingData);

        res.status(201).json({ data: bookingData, status: "success" });
    } catch (err) {
        console.error("Error: ", err);
        res.status(500).json({ error: err.message });
    }
};

async function handleFileUpload(req, res) {
    return new Promise((resolve, reject) => {
        createUpload.upload(req, res, (err) => {
            if (err) {
                console.error("File upload error: ", err);
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function createBookingData(req) {
    return new Bookings({
        email: req.body.email,
        name: req.body.name,
        phoneNo: req.body.phoneNo,
        bookingType: req.body.bookingType,
        bookingStatus: req.body.bookingStatus,
        startingTournamentDate: req.body.startingTournamentDate,
        endingTournamentDate: req.body.endingTournamentDate,
        startingTournamentTime: req.body.startingTournamentTime,
        endingTournamentTime: req.body.endingTournamentTime,
        matchDate: req.body.matchDate,
        SmatchTime: req.body.SmatchTime,
        EmatchTime: req.body.EmatchTime,
        paymentPic: req.file.filename,
    });
}

function logBookingData(bookingData) {
    console.log("email: " + bookingData.email);
    console.log("name: " + bookingData.name);
    console.log("phoneNo: " + bookingData.phoneNo);
    console.log("matchDate: " + bookingData.matchDate);
    console.log("today: " + new Date().setHours(0, 0, 0, 0));

    console.log("compare: " + bookingData.matchDate<new Date().setHours(0, 0, 0, 0));
    // console.log("today: " + new Date().setHours(0, 0, 0, 0));

    // ... (log other properties)
}

async function checkAndCreateBookings(bookingData) {
    if (bookingData.matchDate !== "") {
        await checkTournamentBookings(bookingData);
    } else {
        await checkOtherBookings(bookingData);
    }
}

async function checkTournamentBookings(bookingData) {
    const existingBookings = await Bookings.find({ matchDate: bookingData.matchDate });

    if (existingBookings.length > 0) {
        for (const existingBooking of existingBookings) {
            if (existingBooking.SmatchTime !== null || existingBooking.EmatchTime !== null) {
                let estime = notTime(existingBooking.SmatchTime);
                let eetime = notTime(existingBooking.EmatchTime);
                const ustime = notTime(bookingData.SmatchTime);
                const uetime = notTime(bookingData.EmatchTime);

                if (ustime >= estime && ustime <= eetime || uetime >= estime && uetime <= eetime || ustime <= estime && uetime >= eetime) {
                    return res.status(400).json({ error: "Booking already exists for this date and time" });
                }
            }
        }
    }

    const tournamentBookings = await Bookings.find({ bookingType: "tournament" });

    for (const booking of tournamentBookings) {
        const startingDate = booking.startingTournamentDate;
        const endingDate = booking.endingTournamentDate;

        const allDatesArray = getAllDatesBetween(startingDate, endingDate);

        for (const currentDate of allDatesArray) {
            const formattedDate = currentDate.toISOString().split('T')[0]; // Format the date as "YYYY-MM-DD"
            if (formattedDate == bookingData.matchDate) {
                return res.status(400).json({ error: "Booking already exists for this date and time" });
            }
        }
    }

    await bookingData.save();
}

async function checkOtherBookings(bookingData) {
    const startingTournamentDate = new Date(bookingData.startingTournamentDate);
    const endingTournamentDate = new Date(bookingData.endingTournamentDate);

    const allDatesArray = getAllDatesBetween(startingTournamentDate, endingTournamentDate);

    for (const currentDate of allDatesArray) {
        const formattedDate = currentDate.toISOString().split('T')[0]; // Format the date as "YYYY-MM-DD"

        const existingBookings = await Bookings.findOne({
            $or: [
                { startingTournamentDate: formattedDate },
                { endingTournamentDate: formattedDate },
                { matchDate: formattedDate }
            ]
        });

        if (existingBookings) {
            return res.status(400).json({ error: "Booking already exists for this date and time" });
        }
    }

    await bookingData.save();
}

function notTime(time) {
    const [hrs, mins] = time.split(':');
    return Number(hrs) * 60 + Number(mins); // Convert time to total minutes for easier comparison
}

function getAllDatesBetween(startingDate, endingDate) {
    const datesArray = [];
    const currentDate = new Date(startingDate);

    while (currentDate <= endingDate) {
        datesArray.push(new Date(currentDate));
        currentDate.setDate(currentDate.getDate() + 1);
    }

    return datesArray;
}



exports.getBooking = async (req, res) => {
    try {
        const booking = await Bookings.findById(req.params.id)
        res.json({data: booking, status: "success"})
    } catch (err) {
        res.status(500).json({error: err.message})
    }
}


exports.deleteBooking = async (req, res) => {
    try {
        const booking = await Bookings.findByIdAndDelete(req.params.id)
        res.status(204).json({data: booking, status: "success"})
    } catch (err) {
        res.status(500).json({error: err.message})
    }
}


exports.selectedBookings = async (req, res, next) => {
    console.log("Request received for selected bookings.");

    try {
        const booking = await Bookings.find({ bookingStatus: true }); 
        res.status(200).json({ data: booking, status: "success" });
    } catch (err) {
        console.log(err)
        res.status(500).json({ error: err.message });
    }
};

exports.selectedBookingsDates = async (req, res, next) => {
    console.log("Request received for selected bookings.");

    try {
        const matchDate = new Date(req.params.matchDate);

        const bookings = await Bookings.find({ bookingStatus: true, matchDate: matchDate });

        res.status(200).json({ data: bookings, status: "success" });
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: err.message });
    }
};

exports.notSelectedBookings = async (req, res, next) => {
    try {
        const booking = await Bookings.find({ bookingStatus: false }); 
        res.status(200).json({ data: booking, status: "success" });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};

exports.getAllMyBookings = async (req, res, next) => {
    try {
        const booking = await Bookings.find({ email: req.params.email }); 
        res.status(200).json({ data: booking, status: "success" });
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};




exports.updateBooking = async (req, res) => {
    try {
        const booking = await Bookings.findByIdAndUpdate(req.params.id, req.body)
        res.json({data: booking, status: "success"})
    } catch (err) {
        res.status(500).json({error: err.message})
    }
}

exports.getByDate = async (req, res, next) => {
    try {
        // let data = []; // Use an array to store multiple bookings
        console.log("Ingg")

        const tournamentBookings = await Bookings.find({ bookingType: "tournament", bookingStatus:true});
        console.log(tournamentBookings)

        for (const booking of tournamentBookings) {
            const startingDate = booking.startingTournamentDate;
            const endingDate = booking.endingTournamentDate;
            console.log(booking)
            console.log(startingDate+"     "+endingDate)
    
            const allDatesArray = getAllDatesBetween(startingDate, endingDate);
    
            for (const currentDate of allDatesArray) {
                const formattedDate = currentDate.toISOString().split('T')[0];
                console.log(formattedDate);
                if (formattedDate == req.params.date) {
                    console.log(formattedDate);
                    console.log(booking)
                    return res.status(200).json({ data: booking});
                    
                }
            }
        }

        const matchBooking = await Bookings.find({ matchDate: new Date(req.params.date), bookingStatus:true });
        // data.push(...matchBooking); // Add matchBooking to the array
         console.log("Matchbooking")
         console.log(matchBooking)
       return res.status(200).json({ data: matchBooking});
    
       
    
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
};



