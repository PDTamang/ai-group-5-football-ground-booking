const path = require('path') // path module provides a way of working directories and file paths

// LOG IN PAGE
exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../','view', 'index.html'))
}

// SIGN UP FORM
exports.getResetPasswordForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'view', 'resPassForm.html'))
}
